'use strict';
angular.module('conheca')
.controller('mercados', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosMercados = [];
	$scope.nomeMercado = "";
	$scope.descricao = "";
	$scope.info = null;
	$scope.isMercadoSelected = false;
	carregarPontos();
	
	function carregarPontos(){
		Requisicao.listarMercados(function(res){
	  	console.log(res)
	  	$scope.pontosMercados = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			bairro: res[i].bairro,
		  			info: res[i]

		  		};
		  		$scope.pontosMercados.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}

	$scope.onClick = function(info) {
        $scope.nomeMercado = info.nome;
        $scope.descricao = info.descricao;
        $scope.info = info;
        $scope.isMercadoSelected = true;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("mercadoSelecionado", angular.toJson($scope.info));
		window.location="#/infoMercado";
	}
	

}])
.controller('infoMercado', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var mercadoSelecionado =  angular.fromJson(window.sessionStorage.getItem("mercadoSelecionado"));
	console.log(mercadoSelecionado);
	$scope.nomeMercado = mercadoSelecionado.nome;
	$scope.descricao = mercadoSelecionado.descricao;
	$scope.bairro = mercadoSelecionado.bairro;

	$scope.comentarios = [];
	Requisicao.listarComentarioMercado(mercadoSelecionado.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = mercadoSelecionado.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'mercadoId':idStr
		}

		Requisicao.salvarComentarioMercado(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}

}])

