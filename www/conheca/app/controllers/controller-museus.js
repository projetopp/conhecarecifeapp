'use strict';
angular.module('conheca')
.controller('museus', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosMuseus = [];
	$scope.nomeMuseu = "";
	$scope.descricao = "";
	$scope.info = null;
	$scope.isMuseuSelected = false;
	carregarPontos();

	
		
	
	function carregarPontos(){
		Requisicao.listarMuseus(function(res){
	  	console.log(res)
	  	$scope.pontosMuseus = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			bairro: res[i].bairro,
		  			info: res[i]

		  		};
		  		$scope.pontosMuseus.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}
	$scope.onClick = function(info) {
        $scope.nomeMuseu = info.nome;
        $scope.descricao = info.descricao;
        $scope.info = info;
        $scope.isMuseuSelected = true;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("museuSelecionado", angular.toJson($scope.info));
		window.location="#/infoMuseu";
	}
	

}])
.controller('infoMuseu', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var museuSelecionado =  angular.fromJson(window.sessionStorage.getItem("museuSelecionado"));
	console.log(museuSelecionado);
	$scope.nomeMuseu = museuSelecionado.nome;
	$scope.descricao = museuSelecionado.descricao;
	$scope.localizacao = museuSelecionado.logradouro;
	$scope.bairro = museuSelecionado.bairro;
	$scope.site = museuSelecionado.site;
	$scope.telefone = museuSelecionado.telefone;
	$scope.comentarios = [];
	Requisicao.listarComentarioMuseu(museuSelecionado.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = museuSelecionado.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'museuId':idStr
		}

		Requisicao.salvarComentarioMuseu(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}
	

}])

