'use strict';
angular.module('conheca')
.controller('pontes', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosPontes = [];
	$scope.nomePonte = "";
	$scope.descricao = "";
	$scope.info = null;
	$scope.isPonteSelected = false;
	carregarPontos();
	
	function carregarPontos(){
		Requisicao.listarPontes(function(res){
	  	console.log(res)
	  	$scope.pontosPontes = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			bairro: res[i].bairro,
		  			info: res[i]

		  		};
		  		$scope.pontosPontes.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}

	$scope.onClick = function(info) {
        $scope.nomePonte = info.nome;
        $scope.descricao = info.descricao;
        $scope.info = info;
        $scope.isPonteSelected = true;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("ponteSelecionada", angular.toJson($scope.info));
		window.location="#/infoPonte";
	}
	

}])
.controller('infoPonte', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var ponteSelecionada =  angular.fromJson(window.sessionStorage.getItem("ponteSelecionada"));
	console.log(ponteSelecionada);
	$scope.nomePonte = ponteSelecionada.nome;
	$scope.descricao = ponteSelecionada.descricao;
	$scope.bairro = ponteSelecionada.bairro;

	$scope.comentarios = [];
	Requisicao.listarComentarioPonte(ponteSelecionada.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = ponteSelecionada.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'ponteId':idStr
		}

		Requisicao.salvarComentarioPonte(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}
	

}])

