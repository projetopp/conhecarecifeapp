'use strict';
angular.module('conheca')
.controller('shoppings', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosShoppings = [];
	$scope.nomeShopping = "";
	$scope.descricao = "";
	$scope.info = null;
	$scope.isShoppingSelected = false;
	carregarPontos();
	
	function carregarPontos(){
		Requisicao.listarShoppings(function(res){
	  	console.log(res)
	  	$scope.pontosShoppings = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			bairro: res[i].bairro,
		  			info: res[i]

		  		};
		  		$scope.pontosShoppings.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}

	$scope.onClick = function(info) {
        $scope.nomeShopping = info.nome;
        $scope.descricao = info.descricao;
        $scope.info = info;
        $scope.isShoppingSelected = true;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("shoppingSelecionado", angular.toJson($scope.info));
		window.location="#/infoShopping";
	}
	

}])
.controller('infoShopping', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var shoppingSelecionado =  angular.fromJson(window.sessionStorage.getItem("shoppingSelecionado"));
	console.log(shoppingSelecionado);
	$scope.nomeShopping = shoppingSelecionado.nome;
	$scope.descricao = shoppingSelecionado.descricao;
	$scope.bairro = shoppingSelecionado.bairro;
	$scope.funcionamento = shoppingSelecionado.funcionamento;
	$scope.funcionamentoDomingo = shoppingSelecionado.funcionamentoDomingo;
	$scope.localizacao = shoppingSelecionado.logradouro;
	$scope.site = shoppingSelecionado.site;
	$scope.telefone = shoppingSelecionado.telefone;
	
	$scope.comentarios = [];
	Requisicao.listarComentarioShopping(shoppingSelecionado.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = shoppingSelecionado.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'shoppingId':idStr
		}

		Requisicao.salvarComentarioShopping(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}

}])

