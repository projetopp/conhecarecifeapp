'use strict';
angular.module('conheca')
.controller('teatros', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosTeatros = [];
	$scope.nomeTeatro = "";
	$scope.descricao = "";
	$scope.info = null;
	$scope.isTeatroSelected = false;
	carregarPontos();
	
	function carregarPontos(){
		Requisicao.listarTeatros(function(res){
	  	console.log(res)
	  	$scope.pontosTeatros = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			bairro: res[i].bairro,
		  			info: res[i]

		  		};
		  		$scope.pontosTeatros.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}

	$scope.onClick = function(info) {
        $scope.nomeTeatro = info.nome;
        $scope.descricao = info.descricao;
        $scope.info = info;
        $scope.isTeatroSelected = true;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("teatroSelecionado", angular.toJson($scope.info));
		window.location="#/infoTeatro";
	}
	

}])
.controller('infoTeatro', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var teatroSelecionado =  angular.fromJson(window.sessionStorage.getItem("teatroSelecionado"));
	console.log(teatroSelecionado);
	$scope.nomeTeatro = teatroSelecionado.nome;
	$scope.descricao = teatroSelecionado.descricao;
	$scope.bairro = teatroSelecionado.bairro;
	$scope.telefone = teatroSelecionado.telefone;
	$scope.localizacao = teatroSelecionado.logradouro;

	$scope.comentarios = [];
	Requisicao.listarComentarioTeatro(teatroSelecionado.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = teatroSelecionado.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'teatroId':idStr
		}

		Requisicao.salvarComentarioTeatro(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}
	

}])

