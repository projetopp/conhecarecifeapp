'use strict';
angular.module('conheca')
.controller('feiras', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	
	$scope.map = { center: { latitude:  -8.05428, longitude: -34.8813 }, zoom: 14, id:'1' };
	$scope.options = {icon:'imagens/marker.png'};
	//$scope.pontosFeiras = [];
	$scope.nomeFeira = "";
	$scope.diasAbertos = "";
	$scope.horario = "";
	$scope.observacao = "";
	$scope.info = null;
	$scope.isFeiraSelected = false;
	carregarPontos();
	
	function carregarPontos(){
		Requisicao.listarFeiras(function(res){
	  	console.log(res)
	  	$scope.pontosFeiras = [];
	  	for (var i = 0; res.length > i; i++) {
	  		
	  		if(res[i].latitude !=""){
	  			var latitude = res[i].latitude.replace(",",".");
	  			var longitude = res[i].longitude.replace(",",".");
		  		var ponto ={
		  			latitude:latitude,
		  			longitude:longitude,
		  			icon:'app/images/marker.png',
		  			id:i,
		  			show:false,
		  			nome:res[i].nome,
		  			localizacao: res[i].localizacao,
		  			info: res[i]

		  		};
		  		$scope.pontosFeiras.push(ponto);
	  		}
	  	}
	  },function(){
	  	alert("Erro")
	});
	}

	$scope.onClick = function(info) {
        $scope.nomeFeira = info.nome;
        $scope.diasAbertos = info.diasAbertos;
        $scope.isFeiraSelected = true;
        $scope.horario =info.horario;
        if(info.observacao != ""){
        	$scope.observacao = info.observacao;
        }else{
        	$scope.observacao = "Não há observações";
        }
        $scope.info = info;
    };
	$scope.maisInformacoes = function(){
		window.sessionStorage.setItem("feiraSelecionada", angular.toJson($scope.info));
		window.location="#/infoFeira";
	}
	

}])
.controller('infoFeira', ['$rootScope', '$scope','$http',"Requisicao", function($rootScope, $scope,$http,Requisicao){
	var feiraSelecionada =  angular.fromJson(window.sessionStorage.getItem("feiraSelecionada"));
	console.log(feiraSelecionada);
	$scope.nomeFeira = feiraSelecionada.nome;
	$scope.localizacao = feiraSelecionada.localizacao;
	$scope.diasAbertos = feiraSelecionada.diasAbertos;
	$scope.horario = feiraSelecionada.horario;
	if(feiraSelecionada.observacao != ""){
		$scope.observacao = feiraSelecionada.observacao;
	}else{
		$scope.observacao = "Não há observação"
	}

	$scope.comentarios = [];
	Requisicao.listarComentarioFeira(feiraSelecionada.id,function(res){
	  		$scope.comentarios = res.data;
	  		
		  },function(){
		  	alert("Erro")
		});
	$scope.enviar = function(){
		var id = feiraSelecionada.id;
		var idStr = id.toString();
		var comentario = {
			"nome": $scope.nome,
			"comentario":$scope.comentario,
			'feiraId':idStr
		}

		Requisicao.salvarComentarioFeira(comentario,function(res){
	  		console.log(res)
	  		location.reload();
	  		
		  },function(){
		  	alert("Erro")
		});

	}
	

}])

