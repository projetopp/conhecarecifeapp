var app = angular.module('conheca', ['ngRoute', 'uiGmapgoogle-maps']);
app.config(['$routeProvider', '$httpProvider','uiGmapGoogleMapApiProvider','uiGmapGoogleMapApiProvider' ,function ($routeProvider, $httpProvider,GoogleMapApiProviders,uiGmapGoogleMapApiProvider) {
    GoogleMapApiProviders.configure({
            china: true
        });
    uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyD5NlE-j9QZrnSd7f-sUw9KR_i5GUabRzY',
        v: '3.exp', //defaults to latest 3.X anyhow
        libraries: 'weather,geometry,visualization'
    });
	 $routeProvider
		   .when('/', {
		      templateUrl : 'app/views/home.html',
		      controller     : 'index'
		   })
		   .when('/museus', {
		      templateUrl : 'app/views/museus.html',
		      controller     : 'museus'
		   })
		   .when('/infoMuseu', {
		      templateUrl : 'app/views/infoMuseu.html',
		      controller     : 'infoMuseu'
		   })
		   .when('/shoppings', {
		      templateUrl : 'app/views/shoppings.html',
		      controller     : 'shoppings'
		   })
		   .when('/infoShopping', {
		      templateUrl : 'app/views/infoShopping.html',
		      controller     : 'infoShopping'
		   })
		   .when('/mercados', {
		      templateUrl : 'app/views/mercados.html',
		      controller     : 'mercados'
		   })
		   .when('/infoMercado', {
		      templateUrl : 'app/views/infoMercado.html',
		      controller     : 'infoMercado'
		   })
		   .when('/feiras', {
		      templateUrl : 'app/views/feiras.html',
		      controller     : 'feiras'
		   })
		   .when('/infoFeira', {
		      templateUrl : 'app/views/infoFeira.html',
		      controller     : 'infoFeira'
		   })
		   .when('/teatros', {
		      templateUrl : 'app/views/teatros.html',
		      controller     : 'teatros'
		   })
		   .when('/infoTeatro', {
		      templateUrl : 'app/views/infoTeatro.html',
		      controller     : 'infoTeatro'
		   })
		   .when('/pontes', {
		      templateUrl : 'app/views/pontes.html',
		      controller     : 'pontes'
		   })
		   .when('/infoPonte', {
		      templateUrl : 'app/views/infoPonte.html',
		      controller     : 'infoPonte'
		   })
		   .otherwise ({ redirectTo: '/' });

	    // ferificando se o usuário esta logado no sistema 
	  }
    
]);

