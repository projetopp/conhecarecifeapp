angular.module('conheca')
.factory('Requisicao', ['$http','$rootScope', function($http,$rootScope) {
   
   var ip = "http://54.215.191.231:3234"
   var config = {headers: {
	          'Content-Type': 'application/json',
	          'Accept': 'application/json'
	        }
        }
  

   
   return{
   		listarMuseus:function(success,error){
	        $http.get(ip+'/museus', config).success(success).error(error)
   		},
         listarShoppings:function(success,error){
            $http.get(ip+'/shoppings', config).success(success).error(error)
         },
         listarMercados:function(success,error){
            $http.get(ip+'/mercados', config).success(success).error(error)
         },
         listarFeiras:function(success,error){
            $http.get(ip+'/feiras', config).success(success).error(error)
         },
         listarTeatros:function(success,error){
            $http.get(ip+'/teatros', config).success(success).error(error)
         },
         listarPontes:function(success,error){
            $http.get(ip+'/pontes', config).success(success).error(error)
         },
         salvarComentarioMuseu:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioMuseu', data, config).then(success, error);
         },
         listarComentarioMuseu:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosMuseu/'+data, config).then(success, error);
         },
         salvarComentarioShopping:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioShopping', data, config).then(success, error);
         },
         listarComentarioShopping:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosShopping/'+data, config).then(success, error);
         },
         salvarComentarioMercado:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioMercado', data, config).then(success, error);
         },
         listarComentarioMercado:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosMercado/'+data, config).then(success, error);
         },
         salvarComentarioFeira:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioFeira', data, config).then(success, error);
         },
         listarComentarioFeira:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosFeira/'+data, config).then(success, error);
         },
         salvarComentarioTeatro:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioTeatro', data, config).then(success, error);
         },
         listarComentarioTeatro:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosTeatro/'+data, config).then(success, error);
         },
         salvarComentarioPonte:function(data,success,error){
            $http.post("https://cors-anywhere.herokuapp.com/"+ip+'/comentarioPonte', data, config).then(success, error);
         },
         listarComentarioPonte:function(data,success,error){
            $http.get("https://cors-anywhere.herokuapp.com/"+ip+'/comentariosPonte/'+data, config).then(success, error);
         }
   }
 }]);